library IEEE;
use ieee.std_logic_1164.all;

entity binary_to_gray is
	generic (broj_bitova: Integer := 8);
	port(ulaz: in std_logic_vector(0 to broj_bitova-1);
		izlaz: out std_logic_vector(0 to broj_bitova-1));
end binary_to_gray;

architecture behavioral of binary_to_gray is
begin
	process (ulaz) is
		variable i: integer;
	begin
		i:=0;
		while i<broj_bitova loop
			if i=0 then
				izlaz(i)<=ulaz(i);
			else
				izlaz(i)<=ulaz(i-1) xor ulaz(i);
			end if;
			i:=i+1;
		end loop;
	end process;
end behavioral;
