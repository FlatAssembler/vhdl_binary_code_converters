--An example of how you can call those convertors from your own programs.
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gray_to_bcd is
	port(	ulaz: in std_logic_vector(0 to 7);
			izlaz: out std_logic_vector(0 to 7)
		);
end gray_to_bcd;

architecture structural of gray_to_bcd is
	signal meduspremnik : std_logic_vector(0 to 7);
begin --So, this program converts from Gray code to BCD code...
	s1: entity work.gray_to_binary port map(ulaz,meduspremnik); --...by first invoking "gray_to_binary.vhd", giving it "ulaz" as input and taking its output into "meduspremnik"...
	s2: entity work.binary_to_bcd port map(meduspremnik,izlaz); --...and then invoking "binary_to_bcd.vhd", giving it "meduspremnik" as the input and setting its output to be "izlaz" (the output of this program).
end structural;
