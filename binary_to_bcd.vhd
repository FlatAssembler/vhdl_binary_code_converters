library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity binary_to_bcd is
    Port ( ulaz : in  STD_LOGIC_VECTOR (0 to 7);
           izlaz : out  STD_LOGIC_VECTOR (0 to 7));
end binary_to_bcd;

architecture Behavioral of binary_to_bcd is

begin
	process (ulaz) is
		variable desetice,jedinice: STD_LOGIC_VECTOR (0 to 3);
		variable tmp: Integer;
	begin
		tmp:=to_integer(unsigned(ulaz));
		if tmp>100 or tmp=100 then
			desetice:="1111";
			jedinice:="1111";
		else
			desetice:="0000";
			while tmp>10 or tmp=10 loop
				desetice:=std_logic_vector(to_unsigned(to_integer(unsigned(desetice))+1,4));
				tmp:=tmp-10;
			end loop;
			jedinice:=std_logic_vector(to_unsigned(tmp,4));
		end if;
		izlaz<=desetice&jedinice;
	end process;

end Behavioral;

