library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

entity xs3_to_binary is
    Port ( ulaz : in  STD_LOGIC_VECTOR (0 to 7);
           izlaz : out  STD_LOGIC_VECTOR (0 to 7));
end xs3_to_binary;

architecture Behavioral of xs3_to_binary is

begin
	process (ulaz) is
		variable desetica: STD_LOGIC_VECTOR(0 to 3);
		variable jedinica: STD_LOGIC_VECTOR(0 to 3);
		variable broj: integer;
	begin
		desetica:=ulaz(0 to 3);
		jedinica:=ulaz(4 to 7);
		if to_integer(unsigned(desetica))>2#1100# or to_integer(unsigned(jedinica))>2#1100#
			or (to_integer(unsigned(desetica))>0 and to_integer(unsigned(desetica))<3) or to_integer(unsigned(jedinica))<3
		then
			izlaz<="11111111";
		else
			if desetica="0000" then
				desetica:="0011";
			end if;
			broj:=(to_integer(unsigned(desetica))-3)*10+to_integer(unsigned(jedinica))-3;
			izlaz<=std_logic_vector(to_unsigned(broj,8));
		end if;
	end process;

end Behavioral;

