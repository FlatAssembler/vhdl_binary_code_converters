LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY test_xs3_to_binary IS
END test_xs3_to_binary;
 
ARCHITECTURE behavior OF test_xs3_to_binary IS 
    COMPONENT xs3_to_binary
    PORT(
         ulaz : IN  std_logic_vector(0 to 7);
         izlaz : OUT  std_logic_vector(0 to 7)
        );
    END COMPONENT;
   signal ulaz : std_logic_vector(0 to 7) := (others => '0');

   signal izlaz : std_logic_vector(0 to 7);
BEGIN
    uut: xs3_to_binary PORT MAP (
          ulaz => ulaz,
          izlaz => izlaz
        ); 

   stim_proc: process
	variable ispisDrugi: string(1 to 8);
	variable ispisPrvi: string(1 to 8);
   variable i: integer;
	begin
		ulaz<="00000011";
		while to_integer(unsigned(ulaz))< 2#11001101# loop
			i:=0;
			while i<8 loop
				if izlaz(i)='0' then
					ispisDrugi(i+1):='0';
				else
					ispisDrugi(i+1):='1';
				end if;
				i:=i+1;
			end loop;
			i:=0;
			while i<8 loop
				if ulaz(i)='0' then
					ispisPrvi(i+1):='0';
				else
					ispisPrvi(i+1):='1';
				end if;
				i:=i+1;
			end loop;
			report "XS3 kod sada iznosi: "&ispisPrvi&", a binarni kod sada iznosi: "&ispisDrugi;
			wait for 3 ns;
			ulaz<=std_logic_vector(to_unsigned(to_integer(unsigned(ulaz))+1,8));
		end loop;
      wait;
   end process;

END;
