# VHDL_binary_code_converters

I've made a collection of VHDL programs converting from Gray code to binary code and vice versa, from BCD code to binary code and vice versa, and from XS3 code to binary and vice versa. Maybe it will come useful to somebody.

To see how you can simply include them in your own program, "gray_to_bcd.vhd" is an example for that, it converts from Gray code to BCD code by first calling "gray_to_binary.vhd" and then calling "binary_to_bcd.vhd".
