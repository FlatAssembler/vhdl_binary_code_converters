library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gray_to_binary is
	generic (broj_bitova: Integer := 8);
    Port ( ulaz : in  STD_LOGIC_VECTOR (0 to broj_bitova-1);
           izlaz : out  STD_LOGIC_VECTOR (0 to broj_bitova-1));
end gray_to_binary;

architecture Behavioral of gray_to_binary is
begin
	process (ulaz) is
		variable tmpIzlaz: std_logic_vector(0 to broj_bitova-1);
		variable i: Integer;
	begin
		i:=0;
		while i<broj_bitova loop
			if i=0 then
				tmpIzlaz(i):=ulaz(i);
			else
				tmpIzlaz(i):=tmpIzlaz(i-1) xor ulaz(i);
			end if;
			i:=i+1;
		end loop;
		i:=0;
		while i<broj_bitova loop
			izlaz(i)<=tmpIzlaz(i);
			i:=i+1;
		end loop;
	end process;

end Behavioral;

